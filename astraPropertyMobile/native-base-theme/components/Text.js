import variable from "./../variables/platform";

export default (variables = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize - 1,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    ".welcome": {
      color: "#3d3d3d",
      fontSize: 36,
      fontFamily: "PlayfairDisplay-Regular",
    },
    ".title": {
      color: "#3d3d3d",
      fontSize: 16,
      fontWeight: 'bold',
      letterSpacing: 2,
      paddingBottom: 12,
    },
    ".bold": {
      color: "#3d3d3d",
      fontSize: 16,
      fontWeight: 'bold',
    },
    ".iconTitle": {
      color: "#3d3d3d",
      fontSize: 13,
      fontWeight: 'bold',
      letterSpacing: 0,
      alignSelf:'center',
    },
    ".longText": {
      color:'#3d3d3d',
      fontSize:16,
      letterSpacing:3,
    },
    ".note": {
      color: "#8c8c8c",
      lineHeight: 24,
      fontSize: variables.noteFontSize
    },
    ".longTitle": {
      color: "#3d3d3d",
      fontSize: 17,
      fontWeight: 'bold',
      letterSpacing: 1,
      lineHeight: 24
    },
    ".footTitle": {
      color: "#444444",
      fontSize: 13,
      fontWeight: "500"
    },
    ".footNote": {
      fontSize: 13,
      color: "#8c8c8c",
    },
    ".errorMsg": {
      fontSize: 12,
      color: "#cb625d",
      paddingTop:0,
    },
    ".boxTitle": {
      fontWeight:'500',
      paddingBottom:5
    },
    ".boxText": {
      fontSize:14,
      lineHeight:24,
      color:'#3d3d3d',
    },
    ".popUpMenu": {
      fontSize:20,
      color:'#3d3d3d',
      textAlign:'center',
      padding:20,
    },
    ".popUpTitle": {
      color:'#3d3d3d',
      fontWeight:'500',
      textAlign:'center',
      paddingBottom:6,
    },
    ".popUpRemark" : {
      color:'#868686',
      textAlign:'center',
      fontSize:13,
      lineHeight:20,
    },
    ".miniNote": {
      color:'#868686',
      fontSize:11,
    },
    ".alignRight": {
      alignItems:'flex-end',
      width:'100%',  
    },
    ".listTitle": {
      fontSize: 14,
      fontWeight:'500',
      fontSize:15,
      paddingBottom:5
    },
    ".listText": {
      marginBottom: 10,
      fontSize:12,
      color: '#3d3d3d',
    }
  };

  return textTheme;
};
