import React from 'react';
import { TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { View, Icon, Text } from 'native-base';
import Toast from './../components/Toast';
import moment from 'moment';

const WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
    gold: {
        color:'#71581b',
    },
    goldish:{
        color:'#c8a23f',
    },
    text: {
        color:'#71581b',
        fontSize: 15,
    },
    unit: {
        color:'#71581b',
        fontWeight:'900',
        fontSize:13,
        paddingBottom:10,
    },
    type: {
        color:'#b08c26',
        fontSize:12,
        marginTop:1,
        marginLeft:10,
        flexWrap: 'wrap',
        flex: 1
    },
    time: {
        color:'#71581b',
        fontSize:17,
    },
    border: {
        borderBottomWidth:1,
        borderColor:'#e4b941',
    },
    cardBox: {
        flexDirection: 'row',
    },
    bigBoxEmpty: {
        flexDirection: 'column',
        flex: 4,
        paddingBottom:25,
        paddingTop:25,
        justifyContent: 'center'
    },
    bigBox: {
        flex:4,
        paddingBottom:25,
        paddingTop:10,
        flexDirection: 'column',
    },
    littleBox: {
        flex:1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    centerIcon: {
        justifyContent:'center',
        alignItems:'center',
        width:'100%',
        marginTop:20
    },
    unitIcon: {
        justifyContent:'center',
        alignItems:'center',
        width:'100%',
    },
    roundedBorder: {
        height:48,
        width:48,
        borderWidth:5,
        borderColor:'#fff',
        padding:9,
        borderRadius:25,
        backgroundColor:'#eece70',
        position:'absolute',
        left:6,
        marginTop:25,
        zIndex:2,
        elevation:16
    },
    btnDetail: {
        width: '40%',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:15,
        paddingBottom:15,
        backgroundColor: 'transparent',
        borderColor: "#c69f3d",
        borderWidth: 1,
    },
    textDetail: {
        color: '#c69f3d'
    }
});

class HandoverPick extends React.Component {
    unitList(){
        var unitList = null;
        if (this.props.list.length > 0) {
            unitList = this.props.list.map((item, i) => {
                let listUnit = [];
                listUnit.push(item)
                
                let dateShow = null;
                let linkArrow = null;
                let today = moment().startOf('day');
                if (item.date != null && item.shift != null) {
                    if (today.isSame(moment(item.date))){
                        dateShow = <Text style={styles.time}>Today is your Handover Day, {item.shift}</Text>
                    } else if (today.isAfter(moment(item.date))){
                        dateShow = <Text style={styles.time}>Out of Day, {item.shift}</Text>
                    } else {
                        dateShow = <Text style={styles.time}>{moment(item.date).format('D MMM YYYY')}, {item.shift}</Text>
                        linkArrow = <TouchableOpacity
                            style={styles.unitIcon}
                            onPress={() => this.props._toastMessage()}>
                            <Icon style={styles.goldish} type="FontAwesome" name='angle-right'/>
                        </TouchableOpacity>
                    }
                } else {
                    dateShow = <Text style={styles.time}>No Date Picked</Text>
                    linkArrow = <TouchableOpacity
                        style={styles.unitIcon}
                        onPress={() => this.props.navigation.navigate('HandoverDate', {listUnit : listUnit, dataUnit: item})}>
                        <Icon style={styles.goldish} type="FontAwesome" name='angle-right'/>
                    </TouchableOpacity>
                }
                return (
                    <View verticalRow vertical={true} key={'unit-'+i}>
                        <View horizontalRow horizontal={true}>
                            <View style={styles.roundedBorder}>
                                <Icon white type='FontAwesome' name='key'/>
                            </View>
                            <View padderBox style={{marginBottom: 20,backgroundColor:'#eece70',flexDirection:'column',paddingBottom:0}}>
                                <View horizontalRow horizontal={true} style={styles.cardBox} index={i}>
                                    <View style={styles.bigBox}>
                                        <View horizontalRow horizontal={true}>
                                            <Text style={styles.unit}>{item.unit}</Text>
                                            <Text style={styles.type}>{item.type}</Text>
                                        </View>
                                        {dateShow}
                                    </View>
                                    <View style={styles.littleBox}>
                                        {linkArrow}
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View padder style={{marginBottom: 20}}>
                            <Text note>Payment for this unit hasn’t been paid yet, Check the details for more information.</Text>
                        </View>
                        <View padder style={{marginBottom: 20}}>
                            <TouchableOpacity
                                style={styles.btnDetail}
                                onPress={() => this.props.navigation.navigate('Payment')}>
                                <Text style={styles.textDetail}>View Details</Text>
                            </TouchableOpacity>
                        </View>
                        <View padder style={{marginBottom: 20}}>
                            <View style={{borderTopColor: '#E5E5E5', borderTopWidth: 1}} />
                        </View>
                    </View>
                )
            });
        } else {
            unitList = <View verticalRow vertical={true}>
                <View horizontalRow horizontal={true}>
                    <View style={styles.roundedBorder}>
                        <Icon white type='FontAwesome' name='key'/>
                    </View>
                    <View padderBox style={{marginBottom: 20,backgroundColor:'#eece70',flexDirection:'column',paddingBottom:0}}>
                        <View horizontalRow horizontal={true} style={styles.cardBox}>
                            <View style={styles.bigBoxEmpty}>
                                <Text title>Unit Unavailable</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        }
        return unitList;    
    }

    render() {
        return (<View>
            {this.unitList()}
        </View>)
    }
}

export default HandoverPick;