import React from 'react';
import { StyleSheet } from 'react-native';
import { Card, CardItem, Body, Text, Icon } from 'native-base';

const styles = StyleSheet.create({
    cardIcon: {
        marginBottom: 24,
        marginRight: 10,
        padding: 8,
        width: 135,
    },
    iconCard: {
        color:'#caa64a',
        alignSelf:'center',
        margin:15,
    },
});
  
class CardIcon extends React.Component {
    state = {
        listCard : [
          {
            "id":"1",
            "name":"LOCATION",
            "page":"Location",
            "iconType":"MaterialIcons",
            "iconName":"place",
          },
          {
            "id":"2",
            "name":"FACILITIES",
            "page":"Facility",
            "iconType":"FontAwesome",
            "iconName":"building",
          },
          {
            "id":"3",
            "name":"GALLERY",
            "page":"Gallery",
            "iconType":"FontAwesome",
            "iconName":"image",
          },
        ]    
    };
    
    render() {
        var cardList = [];
        cardList = this.state.listCard.map((item, i) => {
            return (
                <Card style ={styles.cardIcon} key={'card'+i}>
                    <CardItem button onPress={()=>this.props.navigation.navigate(item.page)}>
                        <Body>
                            <Icon style={styles.iconCard} type={item.iconType} name={item.iconName}/>
                            <Text iconTitle>{item.name}</Text>
                        </Body>
                    </CardItem>
                </Card>
            )
        });
        return cardList;
    }
}

export default CardIcon;
