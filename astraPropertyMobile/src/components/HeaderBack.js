import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Header, Left, Body, Right, Text } from 'native-base';
  
class HeaderBack extends React.Component {
    render() {
        return (
            <Header androidStatusBarColor="transparent" iosStatusbar="light-content">
                <Left style={{flex:1}}>
                    <TouchableOpacity
                    onPress={()=>this.props.navigation.goBack()}
                    style={{width:'100%',height:'100%',justifyContent:'center'}}>
                    {this.props.theme=='black' &&
                        <Text style={{color:'#fff',width:'100%'}}>Back</Text>
                    }
                    {this.props.theme==undefined &&
                        <Text style={{width:'100%'}}>Back</Text>
                    }
                    </TouchableOpacity>
                </Left>
                <Body style={{flex:2}}>
                {this.props.title!=undefined &&
                    <Text style={{textAlign:'center',width:'100%'}}>{this.props.title}</Text>
                }
                </Body>
                <Right>
                </Right>
            </Header>
        )
    }
}

export default HeaderBack;
