import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { View, Icon, Text } from 'native-base';

const round = {
    height:38,
    width:38,
    paddingLeft:11,
    paddingTop:9,
    borderRadius:20
}

const list = {
    flexDirection: 'row',
    paddingTop: 18,
    paddingBottom: 15,
    paddingRight: 20,
}

const styles = StyleSheet.create({
    inboxRow: {
        ...list,
        borderColor: '#E5E5E5',
        borderBottomWidth: 1,
        backgroundColor: '#FFFFFF'
    },
    inboxRowNew: {
        ...list,
        borderColor: '#e6ddc4',
        borderBottomWidth: 1,
        backgroundColor: '#FFFBF1'
    },
    iconColumn: {
        marginTop:5,
        flex: 1,
        alignItems: 'center', 
    },
    iconBoxNew: {
        ...round,
        backgroundColor:'#3d3d3d',
        elevation:10  
    },
    iconBox: {
        ...round,
        backgroundColor:'#dddddd',
    }
});

class ListInbox extends React.Component {

    render() {
        return (          
        <TouchableOpacity onPress={() => this.props.navigation.navigate('InboxDetail')}>
            <View style={this.props.item.status == 'new' ? styles.inboxRowNew : styles.inboxRow}>
                <View style={styles.iconColumn}>
                    <View style={this.props.item.status == 'new' ? styles.iconBoxNew : styles.iconBox}>
                        <Icon white type='FontAwesome' name='clock-o' />
                    </View>
                </View>
                <View style={{flex: 4}}>
                    <Text listTitle style={this.props.item.status == 'new' ? {color:'#3d3d3d'} : {color:'#868686'}}>{this.props.item.title}</Text>
                    <Text note style={this.props.item.status == 'new' ? {color:'#3d3d3d',fontSize:14} : {color:'#868686',fontSize:14}}>{this.props.item.message}</Text>
                </View>
            </View>
        </TouchableOpacity>
      )
    }
}

export default ListInbox;