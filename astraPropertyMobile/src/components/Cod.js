import React from 'react';
import { StyleSheet, Image, TouchableOpacity, Clipboard  } from 'react-native';
import { Text, View } from 'native-base';

const styles = StyleSheet.create({
    borderPic: {
        borderWidth:1,
        borderColor:'#e6e6e6',
        borderRadius:20,
        overflow:'hidden',
        width:75,
    },
    pic: {
        height:65,
        width:65,
        resizeMode:'center',
        borderRadius:10,
        margin:5,
    },
    padDetail: {
        paddingLeft:20,
        paddingTop:15,
        width:'70%',
    },
    padAN: {
        paddingTop:5,
    },
    padCopy: {
        paddingTop:20,
    },
    padList: {
        paddingHorizontal:20,
    },
    underline: {
        textDecorationLine:'underline',
        color:'#cc9e1d',
        fontSize:11,
        textAlign:'center'
    }
});

class Cod extends React.Component {

    render() {
        return (
            <View padderTop horizontalRow horizontal={true} style={styles.padList}>
                <View style={styles.borderPic}>
                    <Image source={require('./../assets/images/bank/cod.png')} style={styles.pic}/>
                </View>
                <View style={styles.padDetail}>
                    <Text footTitle>Or you can pay it on the handover day</Text>
                    <Text note style={styles.padAN}>Available w Credit or Debit Card</Text>
                </View>
            </View>
        )
    }
}

export default Cod;