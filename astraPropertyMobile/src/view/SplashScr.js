import React from 'react';
import { Image } from 'react-native';
import splash from './../assets/images/splash.png';
import {
  getAsyncStoreLoad,
  resetNavigation,
}
from './../redux/actions/commonAction';

class SplashScreen extends React.Component {
  static navigationOptions = {header:null}

  state= {
    token: '',
  }

  componentWillMount = () => {
    getAsyncStoreLoad('token',this.handleFirstOpen);
  }

  handleFirstOpen=(token)=>{
    setTimeout(()=>{
      if (token!=null) {
        if (token==''){
          getAsyncStoreSave('token',null,resetNavigation('Welcome',this.props.navigation));
        }
        resetNavigation('Handover',this.props.navigation);
      } else {
        resetNavigation('Welcome',this.props.navigation);
      }
    },1000);
  }

  render() {
    return (
      <Image source={splash} style={{width:'100%',height: '100%',resizeMode: 'cover'}} />
    );
  }
}

export default SplashScreen;
