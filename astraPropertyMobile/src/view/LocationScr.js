import React from 'react';
import { View, Image, Linking, TouchableOpacity } from 'react-native';
import { Container, Text } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import styles from './../styles/location';
import {
  resetNavigation,
}
from './../redux/actions/commonAction';

class LocationScreen extends React.Component {
  static navigationOptions = {header:null}

  goToLogin = () => {
    resetNavigation('Login',this.props.navigation);
  }

  _viewMap = () => {
    let url = "https://www.google.com/maps/place/Anandamaya+Residences/@-6.207255,106.8186419,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69f4032d56273b:0xcd61db7aaf359292!8m2!3d-6.207255!4d106.8208306";
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  render() {
    let pic = require('../assets/images/anandamaya-map.png')
     
    return (
      <Container>
        <HeaderBack navigation={this.props.navigation} />
        <View style={styles.locationRow}>
          <Text title>LOCATION</Text>
          <Text note>Taking pride of place on Jalan Jendral Sudirman at the very heart of the most coveted district in Indonesia.</Text>
        </View>
        <View style={styles.addressRow}>
          <Text title>ANANDAMAYA RESIDENCE</Text>
          <Text note>Jl. Jendral Sudirman Kav 5-6, Jakarta Pusat, DKI Jakarta, Indonesia</Text>
        </View>
        <View style={styles.mapRow}>
          <Image source={pic} style={styles.backgroundImage} />
          <View padder style={styles.contentButton}>
            <View style={styles.space20} />
            <TouchableOpacity style={styles.btnMap} onPress={() => this._viewMap()}>
              <Text style={styles.btnText}>View on Map</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    );
  }
}

export default LocationScreen;
