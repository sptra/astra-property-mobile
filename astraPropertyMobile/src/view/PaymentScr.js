import React from 'react';
import { TouchableOpacity, Modal } from 'react-native';
import { Container, Content, View, Text } from 'native-base';
import styles from './../styles/payment';
import ModalContact from './../components/ModalContact';
import HeaderBack from '../components/HeaderBack';
import ListBank from '../components/ListBank';
import Cod from '../components/Cod';

class PaymentScreen extends React.Component {
  static navigationOptions = {header:null}

  state= {
    modalVisible: false,
    listPayment: [],
  }

  componentWillMount = () => {
    let listPayment = [];
    listPayment = [
      {
        "id":"1",
        "bank":"Permata Virtual Account",
        "accountNumber":"0203940330",
        "image":require('./../assets/images/bank/permata.png'),
        "owner":"PT. Brahmayasa Bahtera",
      },
      {
        "id":"2",
        "bank":"BCA Virtual Account",
        "accountNumber":"0000e204234923",
        "image":require('./../assets/images/bank/bca.png'),
        "owner":"PT. Brahmayasa Bahtera",
      },
      {
        "id":"3",
        "bank":"Mandiri Virtual Account",
        "accountNumber":"32048320842390",
        "image":require('./../assets/images/bank/mandiri.png'),
        "owner":"PT. Brahmayasa Bahtera",
      },
    ]
    this.setState({listPayment});
  }

  paymentList(list) {
    var paymentList = [];
    paymentList = list.map((item, i) => {
      return (
        <ListBank item={item} index={i} key={'payment-'+i}/>
      )
    });
    return paymentList;
  }

  render() {
    return (
      <Container>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={()=>{this.setState({modalVisible:false})}}>
          <View popUpShadow>
            <ModalContact />
            <View popUpModal>
              <TouchableOpacity
                onPress={()=>this.setState({modalVisible:false})}>
                <Text popUpMenu style={{color:'#f36868'}}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <HeaderBack navigation={this.props.navigation} />
        <Content>
          <View padder padderTop>
            <Text title>PAYMENT DETAILS &amp; INFORMATIONS</Text>
            <Text note>Before the handover process, you are needed to complete the following bills :</Text>
          </View>
          <View padder padderTop style={{flexDirection: 'row'}}>
            <Text note style={{flex: 1}}>1. Outsanding Balance</Text>
            <Text title style={{flex: 1}}>Rp. </Text>
          </View>
          <View padder padderTop style={{flexDirection: 'row'}}>
            <Text note style={{flex: 1}}>2. Penalty Fee (if any)</Text>
            <Text title style={{flex: 1}}>Rp. </Text>
          </View>
          <View padder padderTop padderBottom style={{flexDirection: 'row'}}>
            <Text note style={{flex: 1}}>3. Security Deposit</Text>
            <Text title style={{flex: 1}}>Rp. </Text>
          </View>
          <View padderBottom secondary>
            {this.paymentList(this.state.listPayment)}
            <Cod />
          </View>
          <View padder padderBottom secondary>
            <Text note style={{textAlign: 'center'}}>Ignore this if you have made the payments</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

export default PaymentScreen;