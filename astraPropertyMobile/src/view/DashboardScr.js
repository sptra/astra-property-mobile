import React from 'react';
import { Image } from 'react-native';
import { Container, Content, Card, CardItem, Form, Item, Icon, Input, Label, Button, Text } from 'native-base';

class DashboardScreen extends React.Component {
  static navigationOptions = {header:null}

  render() {
    return (
      <Container>
        <Content>
          <Card>
            <Button iconLeft
              onPress={() => this.props.navigation.navigate('Handover')}>
              <Icon name='home'/>
              <Text>Handover</Text>
            </Button>
          </Card>
        </Content>
      </Container>
    );
  }
}

export default DashboardScreen;
