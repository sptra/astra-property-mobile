import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Container, Content, View, Form, Item, Input, Label, Button, Text, Toast } from 'native-base';
import HeaderBack from './../components/HeaderBack';
import endPoint from './../redux/service/endPoint';
import {
  apiCall,
  getAsyncStoreSave,
  resetNavigation,
} from './../redux/actions/commonAction';
import styles from './../styles/login';

const BLUR = "#c8c8c8";
const FOCUS = "#3d3d3d";
const UNSET = "#ececec";
const SET = "#c69f3d";
const ERROR = "#cb625d";
const TIMER = 119;

class OTPScreen extends React.Component {
  static navigationOptions = {header:null}

  number = 4;
  state= {
    phone: '',
    code:'',
    error: false,
    complete:false,
    resend: false,
    timer: TIMER,
    minute: Math.floor(TIMER/60),
    second: TIMER%60 < 10 ? '0'+TIMER%60 : TIMER%60,
  }

  handleInputOnChange = (number) => {
    if (number.length >= 4){
      this.setState({code:number,complete:true,borderColor:SET});
    } else {
      this.setState({complete:false,borderColor:UNSET});
    }
  }

  resend = () =>{
    let min = Math.floor(TIMER/60);
    let sec = TIMER%60 < 10 ? '0'+TIMER%60 : TIMER%60;
    this.setState({resend:false,timer:TIMER,minute:min,second:sec});
    this.countDown();
    const api = endPoint.sendOtp;
    const data = {
      phone_number:this.state.phone,
    }
    apiCall.post(api,data,Toast.show({text: "New OTP is sent",position: "bottom"}));
  }

  submit = () => {
    const api = endPoint.login+'?grant_type=password&password='+this.state.code+'&username='+this.state.phone;
    apiCall.post(api,{},this.callbackSubmit,{headers:{Authorization:endPoint.auth}});
  }

  callbackSubmit = (callback) => {
    if (callback.status==200){
      getAsyncStoreSave('token',callback.data.access_token,resetNavigation('Handover',this.props.navigation));
    } else {
      this.setState({error:true,borderColor:ERROR});
    }
  }

  componentWillMount = () => {
    this.setState({phone:this.props.navigation.state.params.phone});
    this.countDown();
  }

  countDown = () => {
    var countDown = setInterval(()=>{
      this.setState({timer: this.state.timer-1});
      let min = Math.floor(this.state.timer/60);
      let sec = this.state.timer%60 < 10 ? '0'+this.state.timer%60 : this.state.timer%60;
      this.setState({minute:min,second:sec})
      if (this.state.timer==0){
        this.setState({resend:true})
        clearInterval(countDown);
      }
    },1000);
  }

  render() {
    return (
      <Container>
        <HeaderBack navigation={this.props.navigation} />
        <Content>
          <View padder padderTop padderBottom>
            <Text longTitle>Input the verification code</Text>
            <Text longTitle>below</Text>
            <Text note style={{paddingTop:5}}>Please Check your SMS Inbox</Text>
          </View>
          <View padder>
            <Form>
              <View style={styles.view}>
                <Item floatingLabel style={{width:'95%',borderColor:this.state.borderColor}}>
                  <Label style={{color:this.state.labelColor}}>Verification Code</Label>
                <Input
                  keyboardType={'numeric'}
                  onFocus={() => this.setState({labelColor:FOCUS})}
                  onBlur={() => this.setState({labelColor:BLUR})}
                  onChangeText={number => this.handleInputOnChange(number)}
                  style={styles.bigInput}
                />
                </Item>
              </View>
              <View errorBox>
                {this.state.error &&
                <Text errorMsg>Please input the correct code</Text>
                }
              </View>
              <View padderTop horizontalRow horizontal={true} style={styles.footerContainer}>
                <TouchableOpacity
                  disabled = {!this.state.complete}
                  onPress={() => this.submit()}
                  style={!this.state.complete == true ? styles.bigButton : styles.bigButtonSuccess}>
                  <Text style={styles.bigButtonText}>Confirm</Text>
                </TouchableOpacity>
                <View style={styles.sideText}>
                  <Text footTitle>Didn't Get the Code ?</Text>
                  {this.state.resend && 
                  <TouchableOpacity
                    onPress={() => this.resend()}>
                    <Text style={{color:SET,fontWeight:'bold'}}>Resend</Text>
                  </TouchableOpacity>
                  }
                  {!this.state.resend && 
                  <Text note>0{this.state.minute}:{this.state.second}</Text>
                  }
                </View>
              </View>
            </Form>
          </View>
        </Content>
      </Container>
    )
  }
}

export default OTPScreen;
