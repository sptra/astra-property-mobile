import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
  coverContentModal: {
    backgroundColor:'rgba(0,0,0,0.5)', 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  contentModal: {
    backgroundColor: "#fff", 
    width: '90%', 
    height: '85%',
  },
  listSymbol: {
    flex:1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  listContent: {
    flex:9
  },
  descRow: {
    flex: 1, 
    marginTop: 20,
    marginLeft: WIDTH*0.075,
  },
  btnImgView: {
    position: 'absolute', 
    zIndex: 2, 
    backgroundColor: 'rgba(0,0,0,0.5)', 
    width: '100%', 
    height: 80,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  btnImgText: {
    color: '#fff'
  },
  titleModal: {
    flex: 1, 
    marginTop: 20,
  },
  descModal: {
    textAlign: 'justify',
    marginBottom: 20,
  },
  imgThumbnail: {
    width: '100%', 
    height: 200, 
    marginTop:5,
  },
  imgDetail: {
    width: '24.25%',
    height: 80, 
    marginRight: "1%",
    marginTop:5,
  },
  imgDetailLink: {
    width: '100%', 
    height: '100%',
  },
  tabCover: {
    marginTop: 20,
    marginLeft: "2%",
  },
  listRow: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10,
  },
  footerText: {
    alignSelf: 'center',
    color: '#000'
	},
  footerLogin:{
    flex:1,
    marginTop: 10,
    justifyContent: 'center',
    padding: 25,
    borderTopColor: '#E5E5E5',
    borderTopWidth: 1,
  },
  footerModal:{
    flex:1,
		backgroundColor: '#fff',
    justifyContent: 'center',
    // padding: 25,
    borderTopColor: '#E5E5E5',
    borderTopWidth: 1,
  },
  coverDivider: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  divider: {
    width: "90%",
    borderTopColor: '#E5E5E5',
    borderTopWidth: 1,
  }
});
