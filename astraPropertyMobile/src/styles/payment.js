import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  padBox: {
    flexDirection:'column',
    paddingLeft:20,
    paddingRight:20,
    marginBottom:40,
  },
  right: {
    alignItems:'flex-end',
    width:'100%',
  },
  goldBold: {
    color:'#c7a140',
    fontWeight:'bold',
    fontSize: 13,
  },
  padNote: {
    paddingTop:15,
    paddingBottom:30,
  },
});
