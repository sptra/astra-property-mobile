import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  goldBold: {
    color:'#c7a140',
    fontWeight:'bold',
    fontSize: 13,
  },
  roundedBorder: {
    height:48,
    width:48,
    borderWidth:5,
    borderColor:'#fff',
    padding:9,
    borderRadius:25,
    backgroundColor:'#eece70',
    position:'absolute',
    left:6,
    marginTop:25,
    zIndex:2,
    elevation:16
  },
  rounded: {
    height:38,
    width:38,
    padding:9,
    borderRadius:25,
    backgroundColor:'#eece70',
    position:'absolute',
    left:11,
    marginTop:25,
    zIndex:2,
    elevation:16
  },
  right: {
    alignItems:'flex-end',
    width:'100%',
  },
});
