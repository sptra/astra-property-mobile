import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  searchContent: {
    backgroundColor: '#F5F5F5',
    flexDirection: 'column',
    paddingTop: 10,
    paddingBottom: 10,
  },
  searchForm: {
    alignItems: 'center', 
    justifyContent: 'center',
    flexDirection: 'row'
  },
  inboxContent: {
    
  },
  inboxEmptyRow: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: HEIGHT*0.75
  },
  inboxEmptyIcon: {
    width: WIDTH*0.09,
    height: WIDTH*0.09,
    color: '#C7C7C7',
    fontSize: 40
  },
  inboxEmptyText: {
    color: '#C6C6C6',
    marginTop: 20
  },
});
