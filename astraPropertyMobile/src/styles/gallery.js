import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  galleryRow: {
    width: "90%",
    marginLeft: WIDTH*0.050
  },
  tabContent: {
    marginTop: 20,
    marginLeft: "2%",
  }
});
