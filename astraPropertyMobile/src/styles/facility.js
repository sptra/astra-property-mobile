import { StyleSheet, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  backgroundImage: {
    width: "95%",
    height: 470,
    zIndex: -10,
  },
  compassImg: {
    width: 60,
    height: 60,
    position: 'absolute',
    bottom: 90,
    right: 40,
  },
  listFacilityRow: {
    flex: 1, 
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listFacilityItem: {
    marginBottom: 10,
    fontSize: 12
  }
});
