import React from 'react';
import { Root } from 'native-base';
import { StackNavigator } from 'react-navigation';
import SplashScr from '../view/SplashScr';
import WelcomeScr from '../view/WelcomeScr';
import LoginScr from '../view/LoginScr';
import OTPScr from '../view/OTPScr';
import DashboardScr from '../view/DashboardScr';
import HandoverScr from '../view/HandoverScr';
import HandoverDateScr from '../view/HandoverDateScr';
import PaymentScr from '../view/PaymentScr';
import LocationScr from '../view/LocationScr';
import FacilityScr from '../view/FacilityScr';
import GalleryScr from '../view/GalleryScr';
import ProjectScr from '../view/ProjectScr';
import UnitDetailScr from '../view/UnitDetailScr';
import NewsDetailScr from '../view/NewsDetailScr';
import GalleryPopUpScr from '../view/GalleryPopUpScr';
import InboxScr from '../view/InboxScr';
import InboxDetailScr from '../view/InboxDetailScr';

const AppNavigator = StackNavigator({
  Splash: { screen: SplashScr},
  Welcome: { screen: WelcomeScr},
  Login: { screen: LoginScr},
  Otp: { screen: OTPScr},
  Dashboard: { screen: DashboardScr},
  Handover: { screen: HandoverScr},
  HandoverDate: { screen: HandoverDateScr},
  Payment: { screen: PaymentScr},
  Location: { screen: LocationScr},
  Project: { screen: ProjectScr},
  Facility: { screen: FacilityScr},
  Gallery: { screen: GalleryScr},
  Inbox: { screen: InboxScr},
  InboxDetail: { screen: InboxDetailScr},
  GalleryPopUp: { screen: GalleryPopUpScr},
  UnitDetail: { screen: UnitDetailScr},
  NewsDetail: { screen: NewsDetailScr},
}, {
  initialRouteName: 'Splash',
});

export default () => (
  <Root>
    <AppNavigator />
  </Root>
);
